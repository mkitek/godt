package pl.kitek.godt.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.kitek.godt.R;
import pl.kitek.godt.common.models.Recipe;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private List<Recipe> items = new ArrayList<>(0);

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_content, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        Recipe recipe = items.get(position);

        holder.titleText.setText(recipe.getTitle());
        holder.descriptionText.setText(recipe.getDescription());
        Picasso.with(holder.imageView.getContext())
                .load(recipe.getImageUrl())
                .into(holder.imageView);
    }

    @Override public int getItemCount() {
        return (null == items) ? 0 : items.size();
    }

    public Recipe getItem(int position) {
        return items.get(position);
    }

    public void swap(List<Recipe> items) {
        animateTo(items);
    }

    public Recipe removeItem(int position) {
        final Recipe model = items.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, Recipe model) {
        items.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Recipe model = items.remove(fromPosition);
        items.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void animateTo(List<Recipe> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<Recipe> newModels) {
        for (int i = items.size() - 1; i >= 0; i--) {
            final Recipe model = items.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Recipe> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Recipe model = newModels.get(i);
            if (!items.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Recipe> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Recipe model = newModels.get(toPosition);
            final int fromPosition = items.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imageView) ImageView imageView;
        @Bind(R.id.titleText) TextView titleText;
        @Bind(R.id.descriptionText) TextView descriptionText;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
