package pl.kitek.godt.common.sync;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import pl.kitek.godt.common.models.Ingredient;
import pl.kitek.godt.common.models.Recipe;
import timber.log.Timber;

public class RecipeDeserializer implements JsonDeserializer<Recipe> {

    private final Gson gson;

    public RecipeDeserializer() {
        gson = new GsonBuilder()
                .setDateFormat(GodtService.DATETIME_FORMAT)
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }

    @Override public Recipe deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonElement content = json.getAsJsonObject();
        String imageUrl = null;
        try {
            imageUrl = content.getAsJsonObject()
                    .get("images")
                    .getAsJsonArray().get(0)
                    .getAsJsonObject()
                    .get("url").getAsString();
        } catch (Exception e) {
            Timber.e("deserialize error: %s", e.getMessage());
        }

        ArrayList<Ingredient> ingredients = null;
        try {
            JsonArray jsonArray = content.getAsJsonObject()
                    .get("ingredients").getAsJsonArray().get(0)
                    .getAsJsonObject().get("elements")
                    .getAsJsonArray();

            Type listType = new TypeToken<ArrayList<Ingredient>>() {
            }.getType();

            ingredients = new Gson().fromJson(jsonArray, listType);
        } catch (Exception e) {
            Timber.e("deserialize error: %s", e.getMessage());
        }

        Recipe recipe = gson.fromJson(content, Recipe.class);
        recipe.setImageUrl(imageUrl);
        recipe.setIngredients(ingredients);

        return recipe;
    }
}
