package pl.kitek.godt.detail;

import java.sql.SQLException;

import javax.inject.Inject;

import pl.kitek.godt.App;
import pl.kitek.godt.common.db.RecipeMannager;
import pl.kitek.godt.common.models.Recipe;
import timber.log.Timber;

public class DetailPresenter {

    private final int id;
    @Inject RecipeMannager recipeMannager;
    private DetailView view;

    public DetailPresenter(DetailView view, int id) {
        App.inject(this);
        this.id = id;
        this.view = view;
    }

    public void onResume() {
        loadItems();
    }

    private void loadItems() {
        try {
            Recipe recipe = recipeMannager.queryForId(id);
            view.setTitle(recipe.getTitle());
            view.setImageUrl(recipe.getImageUrl());
            view.setDescription(recipe.getDescription());
            view.setIngredients(recipe.getIngredients());
        } catch (SQLException e) {
            Timber.v("loadItems error: %s", e.getMessage());
        }
    }

    public void onDestroy() {
        view = null;
    }
}
