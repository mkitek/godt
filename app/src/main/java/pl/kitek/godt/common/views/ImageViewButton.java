package pl.kitek.godt.common.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import pl.kitek.godt.R;

public class ImageViewButton extends ImageView {

    private static final int FILTER_COLOR_DEFAULT = 0x77000000;

    private int filterColor;
    private boolean hasOnClickListener = false;
    private OnTouchListener customOnTouchListener;
    private OnTouchListener onTouchListener = new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (null != getDrawable()) {
                        getDrawable().setColorFilter(filterColor, PorterDuff.Mode.SRC_ATOP);
                        invalidate();
                    }
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    if (null != getDrawable()) {
                        getDrawable().clearColorFilter();
                        invalidate();
                    }
                    break;
            }

            if (null != customOnTouchListener) {
                customOnTouchListener.onTouch(v, event);
            }

            return !hasOnClickListener;
        }
    };

    public ImageViewButton(Context context) {
        super(context);
        sharedConstructor(context, null);
    }

    public ImageViewButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        sharedConstructor(context, attrs);
    }

    public ImageViewButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        sharedConstructor(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ImageViewButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        sharedConstructor(context, attrs);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        super.setOnClickListener(l);
        hasOnClickListener = null != l;
    }

    @Override
    public void setOnTouchListener(OnTouchListener onTouchListener) {
        this.customOnTouchListener = onTouchListener;
    }

    private void sharedConstructor(Context context, AttributeSet attrs) {
        super.setOnTouchListener(onTouchListener);
        if (null != attrs) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.ImageViewButton,
                    0, 0);
            try {
                filterColor = a.getInteger(R.styleable.ImageViewButton_filterColor, FILTER_COLOR_DEFAULT);
            } catch (Exception e) {
                a.recycle();
            }
        }
    }
}
