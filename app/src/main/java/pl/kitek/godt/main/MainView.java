package pl.kitek.godt.main;

import java.util.List;

import pl.kitek.godt.common.models.Recipe;

public interface MainView {

    int DETAIL_REQUEST = 100;

    void showLoading();

    void hideLoading();

    void navigateToDetail(int id, String title);

    void setItems(List<Recipe> items);

    void showError(String error);
}
