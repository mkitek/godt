package pl.kitek.godt.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.kitek.godt.R;
import pl.kitek.godt.common.models.Recipe;
import pl.kitek.godt.common.views.FixedRecyclerView;
import pl.kitek.godt.common.views.RecyclerItemClickListener;
import pl.kitek.godt.detail.DetailActivity;
import pl.kitek.godt.detail.DetailFragment;

public class MainActivity extends AppCompatActivity implements MainView, SwipeRefreshLayout.OnRefreshListener,
        RecyclerItemClickListener.OnItemClickListener, SearchView.OnQueryTextListener {

    private static final String FRAG_TAG = "detailFrag";
    
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.recycleView) FixedRecyclerView recyclerView;
    @Bind(R.id.swipeContainer) SwipeRefreshLayout swipeRefreshLayout;

    private MainAdapter mainAdapter;
    private SearchView searchView;
    private MainPresenter mainPresenter;
    private boolean isDualPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        isDualPanel = getResources().getBoolean(R.bool.isDualPanel);
        setSupportActionBar(toolbar);
        mainAdapter = new MainAdapter();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, this, false));
        recyclerView.setAdapter(mainAdapter);
        swipeRefreshLayout.setOnRefreshListener(this);

        mainPresenter = new MainPresenter(this, this);
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override protected void onSaveInstanceState(Bundle outState) {
        mainPresenter.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mainPresenter.onRestoreInstanceState(savedInstanceState);
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mainPresenter.onActivityResult(requestCode, resultCode);
    }

    @Override protected void onPause() {
        super.onPause();
        mainPresenter.onPause();
    }

    @Override protected void onResume() {
        super.onResume();
        mainPresenter.onResume();
    }

    @Override protected void onDestroy() {
        mainPresenter.onDestroy();
        super.onDestroy();
    }

    @Override public void onRefresh() {
        mainPresenter.onRefresh();
    }

    @Override public void onItemClick(View view, int position) {
        mainPresenter.onItemClick(mainAdapter.getItem(position));
    }

    @Override public void showLoading() {
        if (null != searchView && !searchView.isIconified()) {
            searchView.setIconified(true);
        }
        swipeRefreshLayout.post(new Runnable() {
            @Override public void run() {
                if (null != swipeRefreshLayout) {
                    swipeRefreshLayout.setRefreshing(true);
                }
            }
        });
    }

    @Override public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override public void navigateToDetail(int id, String title) {
        if (isDualPanel) {
            DetailFragment fragment = DetailFragment.newInstance(id);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.item_detail_container, fragment, FRAG_TAG)
                    .commit();

        } else {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(DetailFragment.EXTRA_ID, id);
            intent.putExtra(DetailFragment.EXTRA_TITLE, title);
            startActivityForResult(intent, DETAIL_REQUEST);
        }
    }

    @Override public void setItems(List<Recipe> items) {
        mainAdapter.swap(items);
    }

    @Override public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override public boolean onQueryTextSubmit(String text) {
        mainPresenter.onQueryTextChange(text);
        return false;
    }

    @Override public boolean onQueryTextChange(String text) {
        mainPresenter.onQueryTextChange(text);
        return false;
    }
}
