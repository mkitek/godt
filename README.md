# Godt #

Simple app for [godt.no](http://www.godt.no/) - Norway's most popular food website.

#### Features:

- fetch and store locally 50 recipes from godt.no
- working offline (after single online fetch)
- support tablets devices (w900dp) with master-detail views
- support searching recipes by ingredients and titles

#### About:

I tried to create a production structure of the project. I used well known patterns like: 

- dependencies injection (dagger)
- simple MVP implementation (views interfaces, presenters with DI, delegate much as possible UI logic from activities/fragments)
- tried to keep proguard updated
- made gradle config for release ready

#### Screens:

![alt text](https://bytebucket.org/mkitek/godt/raw/48835ecddf1f9dd9fd35397687003b55985e45d1/screenshots/device-2016-02-24-020543.png "")

![alt text](https://bytebucket.org/mkitek/godt/raw/48835ecddf1f9dd9fd35397687003b55985e45d1/screenshots/device-2016-02-24-020858.png "")

![alt text](https://bytebucket.org/mkitek/godt/raw/48835ecddf1f9dd9fd35397687003b55985e45d1/screenshots/device-2016-02-24-021040.png "")