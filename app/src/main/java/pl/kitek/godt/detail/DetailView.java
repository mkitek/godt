package pl.kitek.godt.detail;

import java.util.ArrayList;

import pl.kitek.godt.common.models.Ingredient;

public interface DetailView {

    void setImageUrl(String url);

    void setTitle(String title);

    void setDescription(String description);

    void setIngredients(ArrayList<Ingredient> ingredients);
}
