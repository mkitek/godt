package pl.kitek.godt.common.events;

public class FetchFailEvent {
    private final String reason;

    public FetchFailEvent(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }
}
