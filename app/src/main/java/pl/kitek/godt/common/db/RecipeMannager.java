package pl.kitek.godt.common.db;

import android.text.TextUtils;

import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import pl.kitek.godt.common.models.Ingredient;
import pl.kitek.godt.common.models.Recipe;

public class RecipeMannager {

    private static final int MIN_QUERY_LEN = 2;
    private final DatabaseHelper db;

    public RecipeMannager(DatabaseHelper db) {
        this.db = db;
    }

    public List<Recipe> queryForAll() throws SQLException {
        return db.getRecipeDao().queryForAll();
    }

    public List<Recipe> queryFilter(String query) throws SQLException {
        if (TextUtils.isEmpty(query) || query.length() < MIN_QUERY_LEN) {
            return queryForAll();
        }
        String queryLike = String.format(Locale.getDefault(), "%%%s%%", query);
        ArrayList<Integer> ids = new ArrayList<>(0);

        // SELECT id FROM recipe WHERE title LIKE "% query %"
        QueryBuilder<Recipe, Integer> qbRecipe = db.getRecipeDao().queryBuilder();
        qbRecipe.selectColumns(Recipe.ID).where().like(Recipe.TITLE, queryLike);
        for (Recipe recipe : db.getRecipeDao().query(qbRecipe.prepare())) {
            ids.add(recipe.getId());
        }

        // SELECT recipe_id FROM ingredient WHERE name LIKE "%text%"
        QueryBuilder<Ingredient, Integer> qbIngredient = db.getIngredientDao().queryBuilder();
        qbIngredient.selectColumns(Ingredient.RECIPE_ID).where().like(Ingredient.NAME, queryLike);
        for (Ingredient ingredient : db.getIngredientDao().query(qbIngredient.prepare())) {
            ids.add(ingredient.getRecipeId());
        }

        // SELECT * FROM recipe WHERE id IN (...)
        qbRecipe = db.getRecipeDao().queryBuilder();
        qbRecipe.where().in(Recipe.ID, ids);
        return db.getRecipeDao().query(qbRecipe.prepare());
    }

    public Recipe queryForId(int id) throws SQLException {
        Recipe recipe = db.getRecipeDao().queryForId(id);
        recipe.setIngredients((ArrayList<Ingredient>) db.getIngredientDao().queryForEq(Ingredient.RECIPE_ID, id));
        return recipe;
    }
}
