package pl.kitek.godt.common.sync;

import android.app.IntentService;
import android.content.Intent;

import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.squareup.otto.Bus;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import pl.kitek.godt.App;
import pl.kitek.godt.common.db.DatabaseHelper;
import pl.kitek.godt.common.events.FetchFailEvent;
import pl.kitek.godt.common.events.FetchSuccessEvent;
import pl.kitek.godt.common.models.Ingredient;
import pl.kitek.godt.common.models.Recipe;
import timber.log.Timber;

public class SyncService extends IntentService {

    private static final int LIMIT = 50;

    @Inject Bus bus;
    @Inject GodtService service;
    @Inject DatabaseHelper db;
    @Inject NetworkUtils networkUtils;

    private boolean isLoading;

    public SyncService() {
        super("SyncService");
        App.inject(this);
    }

    @Override protected void onHandleIntent(Intent intent) {
        if (!networkUtils.isOnline()) {
            return;
        }

        if (isLoading) {
            return;
        }
        isLoading = true;
        boolean isSuccess = true;

        Timber.i("start syncing...");

        try {
            final ArrayList<Recipe> recipes = service
                    .getRecipesListDetailed("thumbnail-medium", 1, LIMIT, 0)
                    .execute().body();

            try {
                TransactionManager.callInTransaction(db.getConnectionSource(), new Callable<Void>() {
                    @Override public Void call() throws Exception {
                        for (Recipe recipe : recipes) {

                            db.getRecipeDao().createOrUpdate(recipe);

                            DeleteBuilder<Ingredient, Integer> qb = db.getIngredientDao().deleteBuilder();
                            qb.where().eq(Ingredient.RECIPE_ID, recipe.getId());
                            db.getIngredientDao().delete(qb.prepare());

                            for (Ingredient ingredient : recipe.getIngredients()) {
                                ingredient.setRecipeId(recipe.getId());
                                db.getIngredientDao().createOrUpdate(ingredient);
                            }

                        }
                        return null;
                    }
                });
            } catch (SQLException e) {
                Timber.e("SQLException: %s", e.getMessage());
                isSuccess = false;
            }
        } catch (IOException e) {
            Timber.e("IOException: %s", e.getMessage());
            isSuccess = false;
        }
        isLoading = false;
        bus.post(isSuccess ? new FetchSuccessEvent() : new FetchFailEvent("Lorem ipsum"));
    }
}
