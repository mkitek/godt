package pl.kitek.godt.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.kitek.godt.R;
import pl.kitek.godt.common.models.Ingredient;

public class DetailFragment extends Fragment implements DetailView {

    public static final String EXTRA_ID = "id";
    public static final String EXTRA_TITLE = "title";

    @Bind(R.id.imageView) ImageView imageView;
    @Bind(R.id.titleText) TextView titleText;
    @Bind(R.id.descriptionText) TextView descriptionText;
    @Bind(R.id.ingredientText) TextView ingredientText;

    private int id;
    private DetailPresenter detailPresenter;

    public static DetailFragment newInstance(int id) {
        Bundle arguments = new Bundle();
        arguments.putInt(DetailFragment.EXTRA_ID, id);

        DetailFragment detailFragment = new DetailFragment();
        detailFragment.setArguments(arguments);

        return detailFragment;
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        id = getArguments().getInt(EXTRA_ID);
    }

    @Nullable @Override public View onCreateView(LayoutInflater inflater,
                                                 @Nullable ViewGroup container,
                                                 @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        detailPresenter = new DetailPresenter(this, id);
    }

    @Override public void onResume() {
        super.onResume();
        detailPresenter.onResume();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override public void onDestroy() {
        detailPresenter.onDestroy();
        super.onDestroy();
    }

    @Override public void setImageUrl(String url) {
        Picasso.with(getContext())
                .load(url)
                .into(imageView);
    }

    @Override public void setTitle(String title) {
        titleText.setText(title);
    }

    @Override public void setDescription(String description) {
        if (TextUtils.isEmpty(description)) {
            descriptionText.setText("");
            return;
        }
        descriptionText.setText(description.replace("<br />", "\n"));
    }

    @Override public void setIngredients(ArrayList<Ingredient> ingredients) {
        if (null == ingredients || ingredients.size() == 0) {
            ingredientText.setText("");
            return;
        }
        StringBuilder sb = new StringBuilder();
        for (Ingredient ingredient : ingredients) {
            if (ingredient.getAmount() > 0) {
                sb.append(String.format(Locale.getDefault(), "- %d %s %s\n",
                        ingredient.getAmount(), ingredient.getUnitName(), ingredient.getName()));
            } else {
                sb.append(String.format(Locale.getDefault(), "- %s\n", ingredient.getName()));
            }
        }
        ingredientText.setText(sb.toString());
    }
}
