package pl.kitek.godt.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import pl.kitek.godt.App;
import pl.kitek.godt.R;
import pl.kitek.godt.common.db.RecipeMannager;
import pl.kitek.godt.common.events.FetchFailEvent;
import pl.kitek.godt.common.events.FetchSuccessEvent;
import pl.kitek.godt.common.models.Recipe;
import pl.kitek.godt.common.sync.SyncService;
import timber.log.Timber;

public class MainPresenter {

    private static final String LIST_SELECTION = "listSelection";
    private static final String LIST_TITLE = "listTitle";
    private final boolean isDualPanel;

    @Inject Bus bus;
    @Inject RecipeMannager recipeMannager;

    private MainView view;
    private Context context;
    private int itemSelectedPosition = -1;
    private String itemSelectedTitle;

    public MainPresenter(MainView view, Context context) {
        App.inject(this);
        this.view = view;
        this.context = context;
        isDualPanel = context.getResources().getBoolean(R.bool.isDualPanel);
    }

    public void onRefresh() {
        view.showLoading();
        context.startService(new Intent(context, SyncService.class));
    }

    public void onItemClick(Recipe item) {
        itemSelectedPosition = item.getId();
        itemSelectedTitle = item.getTitle();
        view.navigateToDetail(item.getId(), item.getTitle());
    }

    public void onQueryTextChange(String text) {
        try {
            view.setItems(recipeMannager.queryFilter(text));
        } catch (SQLException e) {
            view.showError(context.getString(R.string.error_occurred));
            Timber.e("loadItems error: %s", e.getMessage());
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(LIST_SELECTION, itemSelectedPosition);
        outState.putString(LIST_TITLE, itemSelectedTitle);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        itemSelectedPosition = savedInstanceState.getInt(LIST_SELECTION, -1);
        itemSelectedTitle = savedInstanceState.getString(LIST_TITLE);
    }

    public void onActivityResult(int requestCode, int resultCode) {
        if (requestCode == MainView.DETAIL_REQUEST && resultCode == Activity.RESULT_CANCELED) {
            itemSelectedPosition = -1;
        }
    }

    public void onResume() {
        bus.register(this);
        loadItems();
    }

    private void loadItems() {
        try {
            List<Recipe> recipes = recipeMannager.queryForAll();
            if (recipes.size() > 0) {
                view.setItems(recipes);

                if (isDualPanel && itemSelectedPosition < 0) {
                    itemSelectedPosition = recipes.get(0).getId();
                    itemSelectedTitle = recipes.get(0).getTitle();
                }
                if (itemSelectedPosition > 0) {
                    view.navigateToDetail(itemSelectedPosition, itemSelectedTitle);
                }
            } else {
                onRefresh();
            }
        } catch (SQLException e) {
            view.showError(context.getString(R.string.error_occurred));
            Timber.e("loadItems error: %s", e.getMessage());
        }
    }

    public void onPause() {
        bus.unregister(this);
    }

    public void onDestroy() {
        context = null;
        view = null;
    }

    //region Events
    @Subscribe
    public void onFetchFailEvent(FetchFailEvent event) {
        view.hideLoading();
        view.showError(event.getReason());
    }

    @Subscribe
    public void onFetchSuccessEvent(FetchSuccessEvent event) {
        view.hideLoading();
        loadItems();
    }
    //endregion
}
