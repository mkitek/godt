package pl.kitek.godt;

import android.app.Application;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import dagger.ObjectGraph;
import pl.kitek.godt.common.di.MainModuleAdapter;
import timber.log.Timber;

public class App extends Application {

    private static ObjectGraph objectGraph;

    public static void inject(Object object) {
        objectGraph.inject(object);
    }

    @Override public void onCreate() {
        super.onCreate();

        objectGraph = ObjectGraph.create(new MainModuleAdapter(getApplicationContext()));

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree()); // Enable timber for debug
        }

        // Force picasso to use disc cache
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        Picasso.setSingletonInstance(built);
    }
}
