package pl.kitek.godt.common.di;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import pl.kitek.godt.common.MainThreadBus;
import pl.kitek.godt.common.db.DatabaseHelper;
import pl.kitek.godt.common.db.RecipeMannager;
import pl.kitek.godt.common.models.Recipe;
import pl.kitek.godt.common.sync.GodtService;
import pl.kitek.godt.common.sync.NetworkUtils;
import pl.kitek.godt.common.sync.RecipeDeserializer;
import pl.kitek.godt.common.sync.SyncService;
import pl.kitek.godt.detail.DetailPresenter;
import pl.kitek.godt.main.MainPresenter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(library = true,
        injects = {
                SyncService.class,
                MainPresenter.class,
                DetailPresenter.class
        }
)
public class MainModuleAdapter {
    private final Context context;

    public MainModuleAdapter(Context context) {
        this.context = context;
    }

    @Provides
    public Context provideContext() {
        return context;
    }

    @Singleton
    @Provides
    public OkHttpClient provideOkHtttpClient() {
        return new OkHttpClient();
    }

    @Singleton
    @Provides
    public GodtService provideGodtService() {

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Recipe.class, new RecipeDeserializer())
                .setDateFormat(GodtService.DATETIME_FORMAT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GodtService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(GodtService.class);
    }

    @Provides
    @Singleton
    public DatabaseHelper provideDatabaseHelper(Context context) {
        return new DatabaseHelper(context);
    }

    @Provides
    @Singleton
    public RecipeMannager providePlacesManager(DatabaseHelper db) {
        return new RecipeMannager(db);
    }

    @Singleton
    @Provides
    public Bus provideBus() {
        return new MainThreadBus();
    }

    @Provides
    @Singleton
    public NetworkUtils provideNetworkUtils(Context context) {
        return new NetworkUtils(context);
    }
}
