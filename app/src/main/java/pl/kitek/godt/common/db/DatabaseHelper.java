package pl.kitek.godt.common.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import pl.kitek.godt.common.models.Ingredient;
import pl.kitek.godt.common.models.Recipe;
import timber.log.Timber;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "godt.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<Recipe, Integer> recipeDao;
    private Dao<Ingredient, Integer> ingredientDao;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Recipe.class);
            TableUtils.createTable(connectionSource, Ingredient.class);
        } catch (SQLException e) {
            Timber.e(e.getMessage());
        }
    }

    @Override public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Recipe.class, true);
            TableUtils.dropTable(connectionSource, Ingredient.class, true);

            onCreate(database, connectionSource);
        } catch (SQLException e) {
            Timber.e(e.getMessage());
        }
    }

    public Dao<Recipe, Integer> getRecipeDao() throws SQLException {
        if (recipeDao == null) {
            recipeDao = getDao(Recipe.class);
        }
        return recipeDao;
    }

    public Dao<Ingredient, Integer> getIngredientDao() throws SQLException {
        if (ingredientDao == null) {
            ingredientDao = getDao(Ingredient.class);
        }
        return ingredientDao;
    }

    @Override
    public void close() {
        recipeDao = null;
        ingredientDao = null;
        super.close();
    }
}
