package pl.kitek.godt.common.models;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Locale;

@DatabaseTable(tableName = "ingredient")
public class Ingredient {

    public static final String ID = "id";
    public static final String RECIPE_ID = "recipe_id";
    public static final String NAME = "name";

    @DatabaseField(generatedId = true, columnName = ID) private int id;
    @DatabaseField(canBeNull = false, columnName = RECIPE_ID) private int recipeId;
    @Expose @DatabaseField(canBeNull = false, columnName = NAME) private String name;
    @Expose @DatabaseField(canBeNull = true) private int amount;
    @Expose @DatabaseField(canBeNull = true) private String unitName;

    public Ingredient() {
    }

    public int getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }

    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }

    public String getUnitName() {
        return unitName;
    }

    @Override public String toString() {
        return String.format(Locale.getDefault(), "{name:%s, amount:%d, unitName:%s}", name, amount, unitName);
    }
}
