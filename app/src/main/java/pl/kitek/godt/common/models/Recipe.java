package pl.kitek.godt.common.models;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

@DatabaseTable(tableName = "recipe")
public class Recipe {

    public static final String ID = "id";
    public static final String UPDATED_AT = "updated_at";
    public static final String TITLE = "title";

    @Expose @DatabaseField(id = true, columnName = ID) private int id;
    @Expose @DatabaseField(canBeNull = true, columnName = UPDATED_AT) private Date updatedAt;
    @Expose @DatabaseField(canBeNull = false, columnName = TITLE) private String title;
    @Expose @DatabaseField(canBeNull = true) private String description;
    @Expose @DatabaseField(canBeNull = true) private String imageUrl;
    private ArrayList<Ingredient> ingredients = new ArrayList<>(0);

    public Recipe() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override public boolean equals(Object o) {
        boolean isEquals = false;
        if (null != o && o instanceof Recipe) {
            isEquals = getId() == ((Recipe) o).getId();
        }
        return isEquals;
    }

    @Override public String toString() {
        return String.format(Locale.getDefault(), "{id:%d, title:%s imageUrl:%s}", getId(), getTitle(), getImageUrl());
    }
}
