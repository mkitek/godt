package pl.kitek.godt.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.kitek.godt.R;

public class DetailActivity extends AppCompatActivity {

    @Bind(R.id.toolbar) Toolbar toolbar;

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
        ButterKnife.bind(this);

        if (getResources().getBoolean(R.bool.isDualPanel)) {
            setResult(RESULT_OK);
            finish();
            return;
        }

        setSupportActionBar(toolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        int id = getIntent().getIntExtra(DetailFragment.EXTRA_ID, 0);
        String title = getIntent().getStringExtra(DetailFragment.EXTRA_TITLE);
        if (null != title) {
            getSupportActionBar().setTitle(title);
        }
        if (0 == id) {
            Toast.makeText(this, R.string.recipe_not_found, Toast.LENGTH_LONG).show();
            finish();
        }
        if (null == savedInstanceState) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, DetailFragment.newInstance(id))
                    .commit();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}
