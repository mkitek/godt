package pl.kitek.godt.common.sync;

import java.util.ArrayList;

import pl.kitek.godt.common.models.Recipe;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GodtService {

    String BASE_URL = "http://www.godt.no/api/";
    String DATETIME_FORMAT = "yyyy/MM/dd HH:mm:ss"; // REST date format: 2016/02/19 16:58:19

    @GET("getRecipesListDetailed") Call<ArrayList<Recipe>> getRecipesListDetailed(
            @Query("size") String size,
            @Query("ratio") int ratio,
            @Query("limit") int limit,
            @Query("from") int from
    );
}

